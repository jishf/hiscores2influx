﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;

namespace hiscores2influx
{
    class Program
    {

        private static readonly HttpClient client = new HttpClient();

        static async Task Main(string[] args)
        {
            await GetHiscores();
        }

        private static async Task GetHiscores()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            var hiscoresURL = configuration.GetConnectionString("Hiscores");

            Console.Write(hiscoresURL);

            var hiscoresResult = client.GetStringAsync("https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player=Icarus%20Lived");

            var result = await hiscoresResult;
            Console.Write(result);


        }

        
    }
}
